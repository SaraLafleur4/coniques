#include <iostream>
#include <vector>

#include <Eigen/Dense>

#include "Coords2D.hpp"
#include "Conic.hpp"
#include "Geogebra_render.hpp"

int main(void)
{
    // init vertices
    Coords2D pt1(1.0, -3.0, 1.0),
        pt2(5.0, 1.0, 1.0),
        pt3(-2.0, 5.0, 1.0),
        pt4(4.0, -2.0, 1.0),
        pt5(-5.0, 3.0, 1.0),
        pt6(-4.0, -2.0, 1.0),
        pt7(2.0, 5.0, 1.0),
        pt8(-5.0, -1.0, 1.0),
        pt9(3.0, 6.0, 0.0);
    // init lines
    Coords2D line1(pt3 * pt7),
        line2(pt7 * pt2),
        line3(pt2 * pt4),
        line4(pt1 * pt4),
        line5(pt6 * pt8);

    // put them both in separate vectors
    std::vector<Coords2D> vertex_list = {pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8, pt9};
    std::vector<Coords2D> line_list = {line1, line2, line3, line4, line5};

    // compute the conics they belong to
    Eigen::VectorXd conic_from_vertex = compute_conic_from_vertices(vertex_list);
    Eigen::VectorXd conic_from_line = compute_conic_from_lines(line_list);

    // other conics
    std::vector<Coords2D> with_infinite_vertex_list = {pt1, pt2, pt3, pt4, pt9};
    Eigen::VectorXd conic_infinite = compute_conic_from_vertices(with_infinite_vertex_list);

    Eigen::VectorXd conic_parallel(6);
    conic_parallel << 1., 2., 1., 2., 2., 0.;

    Eigen::VectorXd conic_secant(6);
    conic_secant << 0., 1., 0., 0., 0., 0.;

    // store .html file name in a variable
    const std::string output_conic_from_vertex = "output_conic_from_vertex.html";
    const std::string output_conic_from_line = "output_conic_from_line.html";

    const std::string output_conic_infinite = "output_conic_infinite.html";
    const std::string output_conic_parallel = "output_conic_parallel.html";
    const std::string output_conic_secant = "output_conic_secant.html";

    // render visual output
    geogebra_render(vertex_list, conic_from_vertex, output_conic_from_vertex);
    geogebra_render(vertex_list, conic_from_line, output_conic_from_line);

    geogebra_render(vertex_list, conic_infinite, output_conic_infinite);
    geogebra_render(vertex_list, conic_parallel, output_conic_parallel);
    geogebra_render(vertex_list, conic_secant, output_conic_secant);

    // display output location
    std::cout << "Conic computed from vertex can be found in build/" << output_conic_from_vertex << " file\n";
    std::cout << "Conic computed from line can be found in build/" << output_conic_from_line << " file\n";

    std::cout << "Conic computed from vertex (with a single infinite vertex) can be found in build/" << output_conic_infinite << " file\n";
    std::cout << "Conic corresponding to parallel lines can be found in build/" << output_conic_parallel << " file\n";
    std::cout << "Conic corresponding to secant lines can be found in build/" << output_conic_secant << " file" << std::endl;

    return 0;
}
