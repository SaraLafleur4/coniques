#include <iostream>
#include <vector>

#include <Eigen/Dense>

#include "Conic.hpp"

static Eigen::VectorXd compute_conic(std::vector<Coords2D> &vertex_list)
{
  Eigen::MatrixXd A(5, 6);

  for (size_t i = 0; i < 5; i++)
  {
    A(i, 0) = std::pow(vertex_list[i][0] / vertex_list[i][2], 2);                                // (x/w)^2
    A(i, 1) = (vertex_list[i][0] / vertex_list[i][2]) * (vertex_list[i][1] / vertex_list[i][2]); // (x/w) * (y/w)
    A(i, 2) = std::pow(vertex_list[i][1] / vertex_list[i][2], 2);                                // (y/w)^2
    A(i, 3) = (vertex_list[i][0] / vertex_list[i][2]);                                           // (x/w) * (w/w) = (x/w) * 1
    A(i, 4) = (vertex_list[i][1] / vertex_list[i][2]);                                           // (y/w) * (w/w) = (y/w) * 1
    A(i, 5) = 1;                                                                                 // (w/w)^2 = 1^2
  }

  Eigen::JacobiSVD<Eigen::MatrixXd> svd(A, Eigen::ComputeThinU | Eigen::ComputeFullV);

  Eigen::VectorXd conic_coef = svd.matrixV().rightCols(1);

  return conic_coef;
}

Eigen::VectorXd compute_conic_from_vertices(std::vector<Coords2D> &vertex_list)
{
  return compute_conic(vertex_list);
}

Eigen::VectorXd compute_conic_from_lines(std::vector<Coords2D> &line_list)
{
  Eigen::VectorXd conic_coef = compute_conic(line_list);

  Eigen::MatrixXd C(3, 3);
  C << conic_coef[0], conic_coef[1] / 2, conic_coef[3] / 2,
      conic_coef[1] / 2, conic_coef[2], conic_coef[4] / 2,
      conic_coef[3] / 2, conic_coef[4] / 2, conic_coef[5];

  Eigen::MatrixXd C_inverse(3, 3);
  C_inverse = C.inverse();

  conic_coef << C_inverse(0, 0),
                C_inverse(0, 1) * 2,
                C_inverse(1, 1),
                C_inverse(0, 2) * 2,
                C_inverse(1, 2) * 2,
                C_inverse(2, 2);

  return conic_coef;
}
