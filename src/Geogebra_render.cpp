#include <iostream>
#include <vector>

#include "Geogebra_render.hpp"

void set_viewer(Viewer_conic &viewer)
{
  viewer.set_background_color(250, 250, 255);
  viewer.show_axis(true);
  viewer.show_grid(false);
  viewer.show_value(false);
  viewer.show_label(true);
}

void draw_vertices(Viewer_conic &viewer, const std::vector<Coords2D> vertex_list)
{
  viewer.push_point(vertex_list[0].to_VectorXd(), "p1", 200, 0, 0);
  viewer.push_point(vertex_list[1].to_VectorXd(), "p2", 200, 0, 0);
  viewer.push_point(vertex_list[2].to_VectorXd(), "p3", 200, 0, 0);
  viewer.push_point(vertex_list[3].to_VectorXd(), "p4", 200, 0, 0);
  viewer.push_point(vertex_list[4].to_VectorXd(), "p5", 200, 0, 0);
  viewer.push_point(vertex_list[5].to_VectorXd(), "p6", 200, 0, 0);
  viewer.push_point(vertex_list[6].to_VectorXd(), "p7", 200, 0, 0);
  viewer.push_point(vertex_list[7].to_VectorXd(), "p8", 200, 0, 0);
}

void draw_lines(Viewer_conic &viewer, const std::vector<Coords2D> vertex_list)
{
  viewer.push_line(vertex_list[2].to_VectorXd(), vertex_list[6].to_VectorXd() - vertex_list[2].to_VectorXd(), 200, 200, 0);
  viewer.push_line(vertex_list[6].to_VectorXd(), vertex_list[1].to_VectorXd() - vertex_list[6].to_VectorXd(), 200, 200, 0);
  viewer.push_line(vertex_list[1].to_VectorXd(), vertex_list[3].to_VectorXd() - vertex_list[1].to_VectorXd(), 200, 200, 0);
  viewer.push_line(vertex_list[0].to_VectorXd(), vertex_list[3].to_VectorXd() - vertex_list[0].to_VectorXd(), 200, 200, 0);
  viewer.push_line(vertex_list[5].to_VectorXd(), vertex_list[7].to_VectorXd() - vertex_list[5].to_VectorXd(), 200, 200, 0);
}

void draw_conic(Viewer_conic &viewer, const Eigen::VectorXd conic)
{
  viewer.push_conic(conic, 0, 0, 200);
}

void draw_all(Viewer_conic &viewer, const std::vector<Coords2D> vertex_list, const Eigen::VectorXd conic)
{
  // draw vertices
  draw_vertices(viewer, vertex_list);
  // draw lines
  draw_lines(viewer, vertex_list);
  // draw conic
  draw_conic(viewer, conic);
}

int geogebra_render(std::vector<Coords2D> &vertex_list, Eigen::VectorXd &conic, const std::string filename)
{
  Viewer_conic viewer;

  // viewer options
  set_viewer(viewer);

  // draw all
  draw_all(viewer, vertex_list, conic);

  // render
  viewer.display();        // on terminal
  viewer.render(filename); // generate the output file (to open with your web browser)

  return 0;
}
