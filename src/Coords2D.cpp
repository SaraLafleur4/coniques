#include <iostream>
#include <vector>

#include <Eigen/Dense>

#include "Geogebra_conics.hpp"
#include "Geogebra_render.hpp"

#include "Coords2D.hpp"

// constructors

Coords2D::Coords2D(const size_t size) : m_coords(size) {}
Coords2D::Coords2D(const double x, const double y, const double w) : m_coords(3) { m_coords << x, y, w; }
Coords2D::Coords2D(const Eigen::VectorXd value) : m_coords(value) {}
Coords2D::Coords2D(const Coords2D &v) : m_coords(v.m_coords) {}


// seters

void Coords2D::set_type(std::string v_type)
{
    std::vector<std::string> all_types = {"vertex", "line", "unknown"};
    for (const auto & t : all_types)
    {
        if (v_type == t)
        {
            m_type = v_type;
            return;
        }
    }

    throw std::invalid_argument("Coords2D::set_type: wrong Coords2D type");
}


// operators

Coords2D &Coords2D::operator=(const Coords2D &v)
{
    if (&v == this)
        return *this;

    m_coords = v.m_coords;
    return *this;
}

double &Coords2D::operator[](const std::size_t &i)
{
    return m_coords[i];
}

Coords2D Coords2D::operator+(const Coords2D &v) const
{
    assert(v.size() == this->size() && "ERROR: Coords2D::operator+: incompatible size");

    Coords2D result(size());
    std::transform(m_coords.begin(), m_coords.begin() + size(), v.m_coords.begin(), result.m_coords.begin(), std::plus<double>());

    return result;
}

Coords2D Coords2D::operator-(const Coords2D &v) const
{
    assert(v.size() == this->size() && "ERROR: Coords2D::operator-: incompatible size");

    Coords2D result(size());
    std::transform(m_coords.begin(), m_coords.begin() + size(), v.m_coords.begin(), result.m_coords.begin(), std::minus<double>());

    return result;
}

Coords2D Coords2D::operator-() const
{
    Coords2D result(size());
    for (size_t i = 0; i < size(); ++i)
        result[i] = -m_coords[i];

    return result;
}

Coords2D Coords2D::operator*(const double &value) const
{
    Coords2D result(*this);
    for (size_t i = 0; i < size(); ++i)
        result[i] *= value;

    return result;
}

Coords2D Coords2D::operator*(const Coords2D &v) const
{
    assert(v.size() == this->size() && "ERROR: Coords2D::operator*: incompatible size");

    Coords2D result(3);
    result.m_coords[0] = m_coords[1] * v.m_coords[2] - m_coords[2] * v.m_coords[1];
    result.m_coords[1] = m_coords[2] * v.m_coords[0] - m_coords[0] * v.m_coords[2];
    result.m_coords[2] = m_coords[0] * v.m_coords[1] - m_coords[1] * v.m_coords[0];

    if (this->is_vertex() && v.is_vertex())
        result.set_type("line");
    else if (this->is_line() && v.is_line())
        result.set_type("vertex");
    else
        result.set_type("unknown");

    return result;
}


// other methods

void Coords2D::display() const
{
    for (int i = 0; i < m_coords.size(); ++i)
        std::cout << m_coords[i] << " ";
    std::cout << std::endl;
}

double Coords2D::dot(const Coords2D &v) const
{
    assert(v.size() == this->size() && "ERROR: Coords2D::dot: incompatible size");

    double result = 0.0;
    for (int i = 0; i < m_coords.size(); ++i)
        result += m_coords[i] * v.m_coords[i];

    return result;
}

double Coords2D::norm() const
{
    return sqrt(this->dot(*this));
}

Eigen::VectorXd Coords2D::to_VectorXd() const
{
    return m_coords;
}
