#pragma once

#include <vector>
#include <iostream>

#include <Eigen/Dense>

#include "Coords2D.hpp"

Eigen::VectorXd compute_conic_from_vertices(std::vector<Coords2D> &vertex_list);
Eigen::VectorXd compute_conic_from_lines(std::vector<Coords2D> &line_list);
