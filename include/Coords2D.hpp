#pragma once

#include <vector>
#include <iostream>

#include <Eigen/Dense>

class Coords2D
{
public:
    Eigen::VectorXd m_coords;
    std::string m_type = "vertex";

public:
    // constructors
    Coords2D(const size_t size = 0);                          // using its size
    Coords2D(const double x, const double y, const double w); // using its 3 coordinates
    Coords2D(const Eigen::VectorXd value);                    // copy of existing Eigen::VectorXd
    Coords2D(const Coords2D &v);                                // copy constructor

    // destructor
    ~Coords2D() = default; // calls the destructor of each class attribute

    // quick access to its size
    inline std::size_t size() const { return m_coords.size(); }
    // quick add type
    inline bool is_vertex() const { return m_type == "vertex"; }
    inline bool is_line() const { return m_type == "line"; }

    // seters
    void set_type(std::string v_type);

    // operators
    Coords2D &operator=(const Coords2D &v);
    double &operator[](const std::size_t &i);
    Coords2D operator+(const Coords2D &v) const;
    Coords2D operator-(const Coords2D &v) const;
    Coords2D operator-() const;
    Coords2D operator*(const double &value) const;

    Coords2D operator*(const Coords2D &v) const;

    // other methodes
    void display() const;
    double dot(const Coords2D &v) const;
    double norm() const;
    Eigen::VectorXd to_VectorXd() const;
};
