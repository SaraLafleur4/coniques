#pragma once

#include <vector>
#include <iostream>

#include <Eigen/Dense>

#include "Coords2D.hpp"
#include "Geogebra_conics.hpp"

void set_viewer(Viewer_conic &viewer);
void draw_vertices(Viewer_conic &viewer, const std::vector<Coords2D> vertex_list);
void draw_lines(Viewer_conic &viewer, const std::vector<Coords2D> vertex_list);
void draw_conic(Viewer_conic &viewer, const Eigen::VectorXd conic);
void draw_all(Viewer_conic &viewer, const Coords2D vertex_list, const Eigen::VectorXd conic);

int geogebra_render(std::vector<Coords2D> &vertex_list, Eigen::VectorXd &conic, const std::string filename);
